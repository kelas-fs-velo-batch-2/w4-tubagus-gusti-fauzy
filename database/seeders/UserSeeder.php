<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'username' => 'admin',
            'password' => bcrypt('12345'),
            'name' => 'Tubagus Gusti Fauzy',
            'email' => 'tubagus.gustif@gmail.com',
            'employee_id' => 1,
        ]);
    }
}
