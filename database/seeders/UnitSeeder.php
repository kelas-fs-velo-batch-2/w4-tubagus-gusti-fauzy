<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Unit;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $perusahaan = Unit::create([
            'name' => 'Perusahaan X',
        ]);
        $hrd = Unit::create([
            'name' => 'Direksi HRD',
            'parent_unit_id' => $perusahaan->id,
        ]);

        Unit::create([
            'name' => 'Departemen Penjualan',
            'parent_unit_id' => $perusahaan->id,
        ]);

        Unit::create([
            'name' => 'Bagian Human Resource',
            'parent_unit_id' => $hrd->id,
        ]);

        Unit::create([
            'name' => 'Bagian Career Development',
            'parent_unit_id' => $hrd->id,
        ]);
    }
}
